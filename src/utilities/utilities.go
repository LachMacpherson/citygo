package utilities

import (
  "strings"
  "encoding/json"
)

func CamelCase(str string) string{
	var g []string

	p := strings.Fields(str)

	for _,value := range p {
		g = append(g,strings.Title(value))
	}
	return strings.Join(g," ")
}

func ConvertToLowerTrimmed(str string) string{
	return strings.TrimSpace(strings.ToLower(str))
}

func ConvertStringToJson(result []byte, v interface{}){
  	json.Unmarshal(result, v)
}
