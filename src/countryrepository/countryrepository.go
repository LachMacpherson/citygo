package countryrepository

import (
  "strings"
	"citygo.com/utilities"
  "citygo.com/providers"
)

type CountryRepository struct {
	provider providers.IProvider
}

func NewCountryRepository(provider providers.IProvider) *CountryRepository {
  return &CountryRepository{provider: provider}
}

func (ch *CountryRepository) GetAllCountries() *[]string{
	var stringArray []string
	for _,country_item := range ch.provider.GetCountryDetails() {
		  stringArray = append(stringArray, strings.TrimSpace(country_item.Country))
	 }
	return &stringArray
}

func (ch *CountryRepository) GetAllCapitals() *[]string{
	var stringArray []string
	for _,country_item := range ch.provider.GetCountryDetails() {
		  stringArray = append(stringArray, strings.TrimSpace(country_item.City))
	 }
	return &stringArray
}

func (ch *CountryRepository) ReturnCountry(city_name string) *string{
	var country_result string
	for _,country_item := range ch.provider.GetCountryDetails() {
		 if (strings.ToLower(country_item.City) == strings.ToLower(city_name)){
			  country_result = country_item.Country
				break
		 }
	}
	return &country_result
}

func  (ch *CountryRepository) ReturnFlag(country_name string) *string{
	var flag_result string
	for _,country_item := range ch.provider.GetCountryFlags()  {
		 if (strings.ToLower(country_item.Country) == strings.ToLower(country_name)){
			  flag_result = country_item.Flag_base64
				break
		 }
	}
	return &flag_result
}

func (ch *CountryRepository) ReturnCity(city_name string) *string{
	var city_result string
	for _,country_item := range ch.provider.GetCountryDetails()  {
		 if (strings.ToLower(country_item.Country) == strings.ToLower(city_name)){
			  city_result = country_item.City
				break
		 }
	}
	return &city_result
}

func (ch *CountryRepository) GetTemplateObject() *struct{
	Countries *[]string
	Capitals *[]string
	}{
	v2 := &struct {
		Countries *[]string
		Capitals *[]string
	}{
		ch.GetAllCountries(),
		ch.GetAllCapitals(),
	}
	return v2
}

func (ch *CountryRepository) GetTemplateObjectFromForm(name string, cityIn string ) *struct{
	Country *string
	City *string
	Flag interface{}
	Countries *[]string
	Capitals *[]string
	}{

  newName := name
	if (strings.TrimSpace(cityIn) != ""){
		 nameIn := *ch.ReturnCountry(utilities.ConvertToLowerTrimmed(cityIn))
		 if (nameIn != ""){
			 newName = nameIn
		 }
	}
	nme_out := utilities.CamelCase(newName)
	flag := ch.ReturnFlag(utilities.ConvertToLowerTrimmed(newName))
  city := utilities.CamelCase(*ch.ReturnCity(utilities.ConvertToLowerTrimmed(newName)))

	v := &struct {
		Country *string
		City *string
		Flag interface{}
		Countries *[]string
		Capitals *[]string
	}{
		&nme_out,
		&city,
		*flag,
		ch.GetAllCountries(),
		ch.GetAllCapitals(),
	}
	return v
}
