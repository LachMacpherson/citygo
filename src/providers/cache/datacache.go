package cache

import (
  "citygo.com/types"
)

type DataCache struct {

}
var countries []types.Country
var flags []types.CountryFlag
func NewDataCache()  *DataCache {
	return &DataCache{}
}

func (dc *DataCache) GetCountries() []types.Country{
  return countries
}

func (dc *DataCache) SetCountries(countriesIn []types.Country){
  countries = countriesIn
}

func (dc *DataCache) GetFlags() []types.CountryFlag{
  return flags
}

func (dc *DataCache) SetFlags(flagsIn []types.CountryFlag){
   flags = flagsIn
}
