package providers

import (
  "citygo.com/types"
)

type IProvider interface {
   GetCountryDetails() []types.Country
   GetCountryFlags() []types.CountryFlag
}
