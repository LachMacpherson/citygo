package providers

import (
  "citygo.com/utilities"
  "citygo.com/types"
  "citygo.com/constants"
  "citygo.com/providers/cache"
  "os"
  "fmt"
  "io/ioutil"
)

type FileProvider struct {
   DataCache *cache.DataCache
}

func NewFileProvider(cache *cache.DataCache)  FileProvider {
	return FileProvider{DataCache: cache}
}


func (hp FileProvider) GetCountryDetails() []types.Country{
    if (hp.DataCache.GetCountries() == nil){
      var countriesGlobal []types.Country
      // Open our jsonFile
      fmt.Println("Got here")
    	jsonFile, err := os.Open(constants.COUNTRY_JSON_FILE)
    	// if we os.Open returns an error then handle it
    	if err != nil {
    	    fmt.Println(err)
    	}
    	fmt.Println("Successfully Opened")
    	// defer the closing of our jsonFile so that we can parse it later on
    	defer jsonFile.Close()
    	byteValue, _ := ioutil.ReadAll(jsonFile)
      utilities.ConvertStringToJson([]byte(byteValue),&countriesGlobal)
      hp.DataCache.SetCountries(countriesGlobal)
    }
		return hp.DataCache.GetCountries()
}


func (hp FileProvider) GetCountryFlags() []types.CountryFlag{
    if (hp.DataCache.GetFlags() == nil){
      var flagsGlobal []types.CountryFlag
      // Open our jsonFile
    	jsonFile, err := os.Open(constants.FLAG_JSON_FILE)
    	// if we os.Open returns an error then handle it
    	if err != nil {
    	    fmt.Println(err)
    	}
    	fmt.Println("Successfully Opened")
    	// defer the closing of our jsonFile so that we can parse it later on
    	defer jsonFile.Close()
    	byteValue, _ := ioutil.ReadAll(jsonFile)
      utilities.ConvertStringToJson([]byte(byteValue),&flagsGlobal)
      hp.DataCache.SetFlags(flagsGlobal)
    }
  	return hp.DataCache.GetFlags()
}
