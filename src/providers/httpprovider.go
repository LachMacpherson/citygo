package providers

import (
  "citygo.com/utilities"
  "citygo.com/types"
  "citygo.com/constants"
  "citygo.com/providers/cache"
  "net/http"
  "io"
  "log"
)

type HttpProvider struct {
   DataCache *cache.DataCache
}

func NewHttpProvider(cache *cache.DataCache)  HttpProvider {
	return HttpProvider{DataCache: cache}
}

func (hp HttpProvider) GetCountryDetails() []types.Country{
    if (hp.DataCache.GetCountries() == nil){
      var countriesGlobal []types.Country
      req, err := http.NewRequest("GET", constants.COUNTRY_JSON_URL, nil)
    	req.Header.Set("Content-Type", "application/json")
    	client := &http.Client{}
    	resp, err := client.Do(req)
    	if err != nil {
    			panic(err)
    	}
    	defer resp.Body.Close()
    	bytes, err := io.ReadAll(resp.Body)
    	 if err != nil {
    			 log.Fatal(err)
    	 }
  	 	utilities.ConvertStringToJson([]byte(bytes),&countriesGlobal)
      hp.DataCache.SetCountries(countriesGlobal)
    }
		return hp.DataCache.GetCountries()
}

func (hp HttpProvider) GetCountryFlags() []types.CountryFlag{
    if (hp.DataCache.GetFlags() == nil){
      var flagsGlobal []types.CountryFlag
      req, err := http.NewRequest("GET", constants.FLAG_JSON_URL, nil)
      req.Header.Set("Content-Type", "application/json")
      client := &http.Client{}
      resp, err := client.Do(req)
      if err != nil {
          panic(err)
      }
      defer resp.Body.Close()
      bytes, err := io.ReadAll(resp.Body)
       if err != nil {
           log.Fatal(err)
       }
  		utilities.ConvertStringToJson([]byte(bytes),&flagsGlobal)
      hp.DataCache.SetFlags(flagsGlobal)
    }
  	return hp.DataCache.GetFlags()
}
