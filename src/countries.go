//+build wireinject
package main

import (
	"embed"
	"os"
	"citygo.com/di"
)

//go:embed templates/*
var templates embed.FS
var userInterface bool = false

func main() {
		argsWithoutProg := os.Args[1:]

		port := "9090"
	  if (len(argsWithoutProg) > 0 && argsWithoutProg[0] != ""){
			 port = argsWithoutProg[0]
		}

	  //provider := provider.NewFileProvider()
		//provider := provider.NewHttpProvider()
		//countryRepo := countryrepository.NewCountryRepository(provider)
		//countryService := countryservice.NewCountryService(countryRepo)

		if (userInterface){
		  userInput := di.InitializeInterfaceServer()
			userInput.UserInterfaceCLI()
		}else{
			server := di.InitializeCountryServer(templates, port)
			server.Run()
		}
}
