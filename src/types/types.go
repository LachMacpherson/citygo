package types

type Country struct {
	Country string
	City string
}

type CountryFlag struct {
	Country string
	Flag_base64 string
}
