
//+build wireinject

package di
import (
  "citygo.com/providers"
  "citygo.com/providers/cache"
  "citygo.com/countryservice"
	"citygo.com/countryrepository"
	"citygo.com/countryserver"
	"citygo.com/userinterface"
  "github.com/google/wire"
  "embed"
)
//wire.go

func InitializeInterfaceServer() *userinterface.UserInterfaceServer {
    wire.Build(  wire.Struct(new(cache.DataCache), "*"), providers.NewHttpProvider, wire.Bind(new(providers.IProvider), new(providers.HttpProvider)), countryrepository.NewCountryRepository, countryservice.NewCountryService,userinterface.NewUserInterfaceServer )
    return nil
}

func InitializeCountryServer(templates embed.FS, port string) *countryserver.CountryHttpServer {
  wire.Build( wire.Struct(new(cache.DataCache), "*"), providers.NewHttpProvider, wire.Bind(new(providers.IProvider), new(providers.HttpProvider)), countryrepository.NewCountryRepository, countryservice.NewCountryService,countryserver.NewCountryHttpServer )
    return nil
}

/*
func InitializeInterfaceServer() *userinterface.UserInterfaceServer {
    wire.Build(  wire.Struct(new(cache.DataCache), "*"), providers.NewFileProvider, wire.Bind(new(providers.IProvider), new(providers.FileProvider)), countryrepository.NewCountryRepository, countryservice.NewCountryService,userinterface.NewUserInterfaceServer )
    return nil
}

func InitializeCountryServer(templates embed.FS, port string) *countryserver.CountryHttpServer {
  wire.Build( wire.Struct(new(cache.DataCache), "*"), providers.NewFileProvider, wire.Bind(new(providers.IProvider), new(providers.FileProvider)), countryrepository.NewCountryRepository, countryservice.NewCountryService,countryserver.NewCountryHttpServer )
    return nil
}*/
