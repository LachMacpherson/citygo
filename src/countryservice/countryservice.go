package countryservice

import (
  "citygo.com/countryrepository"
)

type CountryService struct {
  Repository *countryrepository.CountryRepository
}


func NewCountryService(repository *countryrepository.CountryRepository) *CountryService {
  return &CountryService{Repository: repository}
}

func (service *CountryService) GetAllCountries() *[]string{
  return service.Repository.GetAllCountries()
}


func (service *CountryService) GetAllCapitals() *[]string{
  return service.Repository.GetAllCapitals()
}


func (service *CountryService) ReturnCountry(cityName string) *string{
  return service.Repository.ReturnCountry(cityName)
}

func (service *CountryService) ReturnFlag(countryName string) *string{
  return service.Repository.ReturnFlag(countryName)
}

func (service *CountryService) ReturnCity(cityName string) *string{
  return service.Repository.ReturnCity(cityName)
}

func (service *CountryService) GetTemplateObject() *struct{
	Countries *[]string
	Capitals *[]string
}{
  return service.Repository.GetTemplateObject()
}

func (service *CountryService) GetTemplateObjectFromForm(name string, cityIn string) *struct{
	Country *string
	City *string
	Flag interface{}
	Countries *[]string
	Capitals *[]string
}{
  return service.Repository.GetTemplateObjectFromForm(name, cityIn)
}
