package countryserver

import (
	"citygo.com/countryservice"
	"citygo.com/constants"
  "embed"
	"net/http"
  "fmt"
	"html/template"
)


var templates embed.FS

type CountryHttpServer struct{
  templates embed.FS
	port string
  service *countryservice.CountryService
}

func NewCountryHttpServer(templates embed.FS, port string, service *countryservice.CountryService) *CountryHttpServer {
  return &CountryHttpServer{
    templates:     templates,
		port: port,
    service: service,
  }
}

func (c *CountryHttpServer) Run() {

	httpServer := &http.Server{
	    Addr:    ":" + c.port,
	    Handler: c.Handler(),
	}
	fmt.Printf("Starting server...\n")
  err := httpServer.ListenAndServe()
	if err != nil {
	   fmt.Println("ListenAndServe: ", err)
	}
}

func (c *CountryHttpServer) Handler() http.Handler {
  mux := http.NewServeMux()

  mux.HandleFunc("/",c.handleResponse)

  return mux
}


func (c *CountryHttpServer) handleResponse(w http.ResponseWriter, r *http.Request){
	switch r.Method {
	case "GET":
		/*
		p, err := data_form.ReadFile("form.html")
		if err != nil {
		    // TODO: Handle error as appropriate for the application.
		}
		w.Write(p)
		*/
		tmplt2, _ := template.ParseFS(c.templates, constants.TEMPLATES_FOLDER + "/" + constants.COUNTRY_INDEX)
		v2 := c.service.GetTemplateObject()
		tmplt2.Execute(w, v2)

		break
	case "POST":
		// Call ParseForm() to parse the raw query and update r.PostForm and r.Form.
		if err := r.ParseForm(); err != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", err)
			return
		}
		//fmt.Fprintf(w, "Post from website! r.PostFrom = %v\n", r.PostForm)
		name := r.FormValue("name")
		cityIn := r.FormValue("city")
		tmplt, _ := template.ParseFS(c.templates, constants.TEMPLATES_FOLDER + "/"+ constants.COUNTRY_FORM)
		obj  := c.service.GetTemplateObjectFromForm(name, cityIn)
		//Convert string to template.URL to allow it work in the template
		v := &struct {
			Country *string
			City *string
			Flag template.URL
			Countries *[]string
			Capitals *[]string
		}{
			obj.Country,
			obj.City,
			template.URL(obj.Flag.(string)),
			obj.Countries,
			obj.Capitals,
		}
		tmplt.Execute(w, v) //merge template  ‘t’ with content of ‘p’
		//fmt.Fprintf(w, "The capital of %v is %v", name, ReturnCity(strings.ToLower(name)))
	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}
}
