
package userinterface

import (
	"citygo.com/countryservice"
  "fmt"
  "strings"
  "bufio"
  "time"
  "os"
)
/*
var clear map[string]func() //create a map for storing clear funcs

func CallClear() {
    value, ok := clear[runtime.GOOS] //runtime.GOOS -> linux, windows, darwin etc.
		if ok { //if we defined a clear func for that platform:
        value()  //we execute it
    } else { //unsupported platform
        panic("Your platform is unsupported! I can't clear terminal screen :(")
    }
}

func init() {
    clear = make(map[string]func()) //Initialize it
    clear["linux"] = func() {
        cmd := exec.Command("clear") //Linux example, its tested
        cmd.Stdout = os.Stdout
        cmd.Run()
    }
    clear["windows"] = func() {
        cmd := exec.Command("cmd", "/c", "cls") //Windows example, its tested
        cmd.Stdout = os.Stdout
        cmd.Run()
    }
		clear["darwin"] = func() {
				cmd := exec.Command("clear scr") //Windows example, its tested
				cmd.Stdout = os.Stdout
				cmd.Run()
		}
}
*/
type UserInterfaceServer struct{
  service *countryservice.CountryService
}

func NewUserInterfaceServer(service *countryservice.CountryService) *UserInterfaceServer{
  return &UserInterfaceServer{
    service: service,
  }
}

func  (ui *UserInterfaceServer) UserInterfaceCLI() {
	var first string
	fmt.Println("\033[2J")

	for
	{
		time.Sleep(1 * time.Second)
		fmt.Println("Enter the country: ")
		reader := bufio.NewReader(os.Stdin)
		// ReadString will block until the delimiter is entered
		input, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("An error occured while reading input. Please try again", err)
			return
		}
		first = strings.TrimSuffix(input, "\n")
		if (strings.ToLower(first) == `x`){
			break
		}
		result := ui.service.ReturnCity(first)
		fmt.Println("\033[2J")
		fmt.Printf("\nThe capital of %s is %s\n", first, *result)
	}
}
