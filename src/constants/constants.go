package constants


const TEMPLATES_FOLDER = "templates"

const COUNTRY_INDEX string = "form.html"
const COUNTRY_FORM string = "IndexTemplated.html"

const COUNTRY_JSON_URL = `https://bitbucket.org/LachMacpherson/citygo/raw/HEAD/src/files/countries.json`
const FLAG_JSON_URL = `https://bitbucket.org/LachMacpherson/citygo/raw/HEAD/src/files/flags.json`

const COUNTRY_JSON_FILE = "files/countries.json"
const FLAG_JSON_FILE = "files/flags.json"
